const Formatter = new Intl.NumberFormat('id-ID',
    {
        'style': 'currency',
        'currency': 'IDR',
        'minimumFractionDigits': 0,
        'maximumFractionDigits': 2
    }
);

let KgTag = u('p#KgTag');
let EkorTag = u('p#EkorTag');

KgTag.html(Formatter.format(parseInt(kgPrize, 10)));
EkorTag.html(Formatter.format(parseInt(ekorPrize, 10)));
