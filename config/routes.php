<?php
/**
 * Routes configuration.
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * It's loaded within the context of `Application::routes()` method which
 * receives a `RouteBuilder` instance `$routes` as method argument.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

/*
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 */
/**
 * @var \Cake\Routing\RouteBuilder $routes
*/
$routes->setRouteClass(DashedRoute::class);

$routes->scope(
    '/',
    function (RouteBuilder $builder) {
        // Register scoped middleware for in scopes.
        $builder->registerMiddleware(
            'csrf',
            new CsrfProtectionMiddleware(
                [
                'httpOnly' => true,
                ]
            )
        );

        /*
        * Apply a middleware to the current route scope.
        * Requires middleware to be registered through `Application::routes()` with `registerMiddleware()`
        */
        $builder->applyMiddleware('csrf');

        /*
        * Here, we are connecting '/' (base path) to a controller called 'Pages',
        * its action called 'display', and we pass a param to select the view file
        * to use (in this case, templates/Pages/home.php)...
        */
        $builder->connect('/', ['controller' => 'Client', 'action' => 'index']);
        $builder->connect('/pesanan', ['controller' => 'Client', 'action' => 'order']);
        $builder->get('/tentang-kami', ['controller' => 'Client', 'action' => 'tentangKami']);

        $builder->fallbacks(DashedRoute::class);
    }
);

$routes->prefix(
    'Guard',
    function (RouteBuilder $builder) {
        $builder->registerMiddleware('csrf', new CsrfProtectionMiddleware([ 'httpOnly' => true, ]));

        $builder->applyMiddleware('csrf');

        $builder->redirect('/', ['controller' => 'Pesanan', 'action' => 'load', 'prefix' => 'Guard']);

        $builder->connect('/login', ['controller' => 'Guard', 'action' => 'login', 'prefix' => 'Guard'])
            ->setMethods(['GET','POST']);

        $builder->get('/logout', ['controller' => 'Guard', 'action' => 'logout', 'prefix' => 'Guard']);

        $builder->connect('/seo', ['controller' => 'Seo', 'action' => 'load', 'prefix' => 'Guard'])
            ->setMethods(['GET','POST']);

        $builder->connect('/prize', ['controller' => 'Prize', 'action' => 'index', 'prefix' => 'Guard'])
            ->setMethods(['GET','POST']);

        $builder->prefix(
            'Pesanan',
            function (RouteBuilder $routes) {
                $routes->get('/', ['controller' => 'Pesanan', 'action' => 'load', 'prefix' => 'Guard'], 'pesananLoad');
                $routes->get('/process/{uid}', ['controller' => 'Pesanan', 'action' => 'nextProcess', 'prefix' => 'Guard/Pesanan'], 'pesananNext')
                    ->setPass(['uid']);
                $routes->get('/delete/{uid}', ['prefix' => 'Guard', 'controller' => 'Pesanan', 'action' => 'remove'], 'pesananRemove')
                    ->setPass(['uid']);
            }
        );

        $builder->prefix(
            'Users',
            function (RouteBuilder $routes) {
                $routes->get('/', ['controller' => 'Users', 'action' => 'load', 'prefix' => 'Guard'], 'user');
                $routes->post('/', ['controller' => 'Users', 'action' => 'add', 'prefix' => 'Guard'], 'adduser');
                $routes->post('/update-password', ['controller' => 'Users', 'action' => 'changePassword', 'prefix' => 'Guard']);
                $routes->get('/delete/{username}', ['controller' => 'Users', 'action' => 'delete', 'prefix' => 'Guard'], 'removeuser')
                    ->setPass(['username'])
                    ->setPatterns(['username' => '[^a-z_\-0-9]+']);
                $routes->get('/toggle/{username}', ['controller' => 'Users', 'action' => 'toggle', 'prefix' => 'Guard'], 'toggleuser')
                    ->setPass(['username'])
                    ->setPatterns(['username' => '[^a-z_\-0-9]+']);
            }
        );

        $builder->fallbacks(DashedRoute::class);
    }
);

/*
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * $routes->scope('/api', function (RouteBuilder $builder) {
 *     // No $builder->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
