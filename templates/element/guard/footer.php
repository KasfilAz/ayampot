<section class="footer">
  <div class="container">
    <div class="content has-text-right">
      <p>
        Website Copyright oleh <strong>Sentral Ayam <?= date("Y"); ?></strong>
      </p>
    </div>
  </div>
</section>
