<?= $this->element('guard/navbar'); ?>
<section class="section">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-8">
        <p class="title has-text-dark has-text-left">Meta tag untuk SEO</p>
        <hr>
        <?= $this->Form->create($seoForm, ['controller' => 'Seo', 'action' => 'load', 'prefix' => 'Guard']); ?>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('description', 'Deskripsi', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->textarea('description', ['class' => 'textarea has-fixed-size', 'id' => 'desc']); ?>
              </div>
              <?php if (isset($formErrors) && !is_null($formErrors['description'])): ?>
              <p class="help is-danger"><?= $formErrors['description']; ?></p>
              <?php else: ?>
              <p class="help">Maksimal 150 karakter</p>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('keywords', 'Keywords', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->textarea('keywords', ['class' => 'textarea has-fixed-size']); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('robots', 'Robots', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->text('robots', ['class' => 'input']); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('revisit', 'Revisit After', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->text('revisit', ['class' => 'input']); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('canonical', 'Canonical', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->text('canonical', ['class' => 'input']); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
            <?= $this->Form->label('analytic_key', 'Google Analytics Key', ['class' => 'label is-normal']); ?>
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control is-expanded">
                <?= $this->Form->text('analytic_key', ['class' => 'input']); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="field is-horizontal">
          <div class="field-label">
          </div>
          <div class="field-body">
            <div class="field">
              <div class="control">
                <input class="button is-danger" type="submit" value="Perbarui Data">
              </div>
            </div>
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</section>
<?= $this->element('guard/footer'); ?>
