<?php
$this->layout = 'error';
$this->assign('title', 'Maaf, Halaman tidak ditemukan');

$this->append('css');
echo $this->Html->css('error');
$this->end();
?>
<section class="hero is-light is-fullheight">
  <div class="container has-text-centered">
    <div class="columns is-centered">
      <div class="column is-4">
        <?= $this->Html->image('logo/logo-dark-1v1.png', ['alt' => 'Sentral Ayam Logo', 'class' => 'error-logo']) ?>
        <p class="title has-text-grey">Maaf,</p>
        <p class="subtitle has-text-grey">Kami tidak menemukan halaman yang kamu cari. <i class="fas fa-frown"></i></p>
        <a class="button is-text" href="/">Kembali Ke Beranda</a>
      </div>
    </div>
  </div>
</section>
