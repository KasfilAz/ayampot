<?php echo $this->element('client/navbar'); ?>
<section class="hero is-fullheight-with-navbar">
  <div class="hero-body">
    <div class="columns">
      <div class="column is-6" id="with-bg">
        <h2 class="has-text-centered">
          <?php echo $this->Html->image('logo/logo-white-1v1.png'); ?>
          <h2 class="title has-text-light">Sentral Ayam</h2>
        </h2>
      </div>
      <div class="column is-6">
        <h3 class="title has-text-left">Tentang Kami</h3>
        <p class="has-text-dark about">
          <a href="/" class="has-text-weight-bold">Sentral Ayam</a> adalah supplier daging ayam segar terbaik di Tangerang dan Jakarta. Kami melayani berbagai kebutuhan daging ayam seperti karkas ataupun daging ayam utuh. Siap mengirim dan memasok kebutuhan daging ayam ke tempat anda baik di Tangerang, Bandung, Depok, Bekasi, Bogor atau kota lainnya di Indonesia. Menyediakan pasokan ayam karkas dan ayam utuh.
        </p>
        <p class="has-text-dark about">
          Dari ayam broiler pilihan daging segar dipotong langsung dan halal. Saat ini <strong>Sentral Ayam</strong> melihat potensi penjualan atau pemasaran lewat internet, kami mencoba memperluas pasar penjualan produk daging ayam segar ke kota besar seperti Jakarta dan sekitarnya.
        </p>
      </div>
    </div>
  </div>
</section>

<style>
#with-bg {
  background: url(../img/bg/about-bg.png);
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;
  align-items: center;
}

p.about {
  margin: 12px 0;
}

#with-bg img {
  width: 200px;
  height: auto;
}

.column {
  flex-direction: column;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  padding: 50px;
}

.hero-body {
  padding: 0;
}

.columns {
  min-height: calc(100vh - 3.25rem);
  min-width: 100%;
}
</style>
