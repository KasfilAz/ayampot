<?php

namespace App\Controller\Guard;

use App\Controller\AppController;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{
    protected $usersTable;

    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('guard');
        $this->usersTable = TableRegistry::getTableLocator()->get('Users');
    }

    public function load(): void
    {
        $query = $this->usersTable->find()->select(['id', 'username', 'is_active'])->all();
        $this->set('users', $query);

        $this->render('load');
    }

    public function add(): void
    {
        $userEntity = $this->usersTable->newEntity($this->request->getData());

        if ($userEntity->getErrors()) {
            $this->Flash->bNotifError('<strong>Ada kesalahan dalam pengisian form!</strong>');
            $this->set('errors', $userEntity->getErrors());

        } else {

            $this->usersTable->save($userEntity);

            $this->Flash->bNotifSuccess('<strong>Berhasil Menambah user</strong>');
        }

        $this->setAction('load');
    }

    public function delete(string $username): Response
    {
        $user = $this->usersTable->find()->where(['username' => $username])->first();

        if ($user !== null) {
            $this->usersTable->delete($user);
        }

        return $this->redirect([
            'controller' => 'Users',
            'action' => 'load'
        ]);
    }

    public function changePassword(): Response
    {
        $newPassword = $this->request->getData('password');
        $identity = $this->Authentication->getIdentity();

        $user = $this->usersTable->get($identity->getIdentifier());
        $user->password = $newPassword;

        if ($this->usersTable->save($user)) {
            $this->Flash->bNotifSuccess('Update password berhasil');
        } else {
            $this->Flash->bNotifError('Gagal update password');
        }

        return $this->redirect([
            'controller' => 'Users',
            'action' => 'load'
        ]);
    }

    public function toggle(string $username): Response
    {
        $user = $this->usersTable->find()->where(['username' => $username])->first();

        if ($user->is_active === true) {
            $user->is_active = false;
        } else {
            $user->is_active = true;
        }

        $this->usersTable->save($user);

        return $this->redirect([
            'controller' => 'Users',
            'action' => 'load'
        ]);
    }
}
