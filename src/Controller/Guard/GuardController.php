<?php
declare(strict_types = 1);

namespace App\Controller\Guard;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class GuardController extends AppController
{
    protected $Users;

    public function initialize(): void
    {
        parent::initialize();

        $this->Authentication->allowUnauthenticated(['login']);
        $this->Users = TableRegistry::getTableLocator()->get('Users');
        $this->viewBuilder()->setLayout('guard');
    }

    public function login()
    {
        $result = $this->Authentication->getResult();
        // If the user is logged in send them away.
        if ($result->isValid()) {
            $target = $this->Authentication->getLoginRedirect() ?? '/guard/pesanan';
            return $this->redirect($target);
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->set('error', 'Password salah atau akun tidak ditemukan');
        }
    }

    public function logout()
    {
        $this->Authentication->logout();
        return $this->redirect('/');
    }
}
