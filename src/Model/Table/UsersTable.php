<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config): void
    {
        $this->setTable('users');
        $this->setEntityClass('App\Model\Entity\User');

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'CREATED_AT' => 'new'
                ]
            ]
        ]);
    }

    public function validationDefault(Validator $validator): Validator
    {
        // 'username' column validator
        $validator->allowEmptyString('username', 'Username tidak boleh kosong', false)
                  ->add('username', 'is_unique', [
                      'rule' => 'validateUnique',
                      'message' => 'Username telah digunakan',
                      'provider'=> 'table'
                  ])
                  ->alphaNumeric('username', 'username hanya boleh berisi angka dan alfabet saja')
                  ->lengthBetween('username', [3, 64], 'username min 3 karakter max 64 karakter');

        // 'password' column validator
        $validator->allowEmptyString('password', 'Password tidak boleh kosong', false);

        // 'email' column validator
        $validator->allowEmptyString('email')
                  ->email('email', 'email harus valid atau kosong');

        return $validator;
    }

    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->addCreate(new IsUnique(['username']));

        return $rules;
    }

    public function findActive(Query $query)
    {
        return $query->where(['is_active' => true]);
    }
}
